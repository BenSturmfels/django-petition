========
Petition
========

Petition is an app to collect signatures to an online petition and if desired,
opt-in for updates on the petition result. This is *extremely* basic in that it
currently only handles one petition at a time.

Petition signatures go into the table ``petition_signatures``.

Requirements
------------

* Django >= 1.3

* a database configured

* Django Admin enabled so you can add events and review signatures.


Quick start
-----------

1. Add Petition to your settings.INSTALLED_APPS::

       INSTALLED_APPS = (
           ...
           'petition',
           ...
       )

2. Include Petition in your project urls.py::

       url(r'^petition/', include('petition.urls')),

3. Run `python manage.py syncdb` to create the Petition models.

4. Start the development server and visit http://127.0.0.1:8000/petition/.
