from django.db import models

STATES = ('ACT', 'NSW', 'NT', 'QLD', 'SA', 'TAS', 'VIC', 'WA')
STATE_CHOICES = [(i, i) for i in STATES]

class Signature(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    name = models.CharField("Full name", max_length=100)
    occupation = models.CharField(help_text="Please be specific eg. Student (Computer Science)", max_length=100)
    email = models.EmailField()
    city = models.CharField("City or suburb", max_length=100)
    state = models.CharField(choices=STATE_CHOICES, max_length=3)
    updates = models.BooleanField("Send me updates on this issue", default=True)

    def first_name(self):
        if self.name.split(' ')[0].lower() not in ('dr', 'dr.'):
            return self.name.split(' ')[0].title()
        else:
            return self.name.split(' ')[1].title()
