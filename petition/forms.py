from django.forms import ModelForm

from .models import Signature

class SignatureForm(ModelForm):
    error_css_class = 'error'

    class Meta:
        model = Signature
