from django.conf.urls.defaults import *

urlpatterns = patterns('petition.views',
    (r'^$', 'petition'),
    (r'^thanks/$', 'thanks'),
)
