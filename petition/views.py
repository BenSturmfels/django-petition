from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.text import wrap

from .forms import SignatureForm
from .models import Signature

@transaction.commit_on_success
def petition(request):
    count = Signature.objects.all().count()
    if request.method == 'POST':
        form = SignatureForm(request.POST)
        if form.is_valid():
            sig = form.save()
            sender = settings.DEFAULT_FROM_EMAIL
            to = form.cleaned_data['email']
            subject = "Thanks for signing!"
            body = wrap(
                render_to_string('petition/email_thanks.html',
                                 {'first_name': sig.first_name()}),
                70)
            send_mail(subject, body, sender, [to], fail_silently=False)
            return HttpResponseRedirect(reverse('petition.views.thanks'))
    else:
        form = SignatureForm()
    return render_to_response('petition/petition.html',
                              {'form': form,
                               'count': count},
                              context_instance=RequestContext(request))


def thanks(request):
    return render_to_response('petition/thanks.html',
                              context_instance=RequestContext(request))
